// SystemODE.cpp: ���������� ����� ����� ��� ����������� ����������.
//

#include "stdafx.h"
#include <iostream>
#include <conio.h>
#include <vector>
#include <stdio.h>
#include <omp.h>
#include <time.h> 
#include <windows.h>
#include <iomanip>
#include "globalUtils.h"

using namespace std;

int main()
{
	setlocale(LC_ALL, "rus");
	system("color F1");
	system("cls");
	srand((unsigned)time(NULL));
	HANDLE hStdout = GetStdHandle(STD_OUTPUT_HANDLE);

	// �����������
	coutWelcome();
	_getch();

	size_t N = 0;
	double **matrixA = 0, **matrixAB = 0;
	double *X = 0, *X0 = 0, *K1 = 0, *K2 = 0;
	double t0 = 0, t1 = 0, dt = 0;

	// ������ � �������������
	while (true)
	{
		system("cls");
		SetConsoleTextColor(hStdout, FOREGROUND_BLUE);
		cout << ">��������, ����� �������� �� ������� ��������� ������� ������.\n";
		SetConsoleTextColor(hStdout, FOREGROUND_GREEN);
		cout << "\n\t1. ��������� �������";
		cout << "\n\t2. ��������� � �����";
		cout << "\n\t3. ��������� ��������";
		SetConsoleTextColor(hStdout, FOREGROUND_BLUE);

		int downloadMode = getMode(3);

		switch (downloadMode)
		{
		case 1:
			// enter matrix
			cout << "\n\n>��������� ������� ������. ��� ������ ���������� ������� �-�� ���������� X.";
			cout << "\n\n ������� �-�� ���������� X � ������� ==> ";

			N = getNumber();

			matrixA = new double*[N];
			for (size_t j = 0; j < N ; j++)
			{
				matrixA[j] = new double[N];
			}
			cout << "\n ��������� ������� ������� ����������:\n ";
			enterMatrix(N, matrixA);

			SetConsoleTextColor(hStdout, FOREGROUND_BLUE);
			cout << "\n ������ ���������� ��������� ��������� �������:";
			cout << "\n t0 ==> ";
			SetConsoleTextColor(hStdout, FOREGROUND_GREEN);
			t0 = getDouble();

			SetConsoleTextColor(hStdout, FOREGROUND_BLUE);
			cout << "\n ��������� ��������� �������� ����������:\n ";
			X0 = new double[N];
			enterX0(N, X0);			

			break;

		case 2:
			cout << "\n ���������  . . .\n ";

			// file
			while (true)
			{
				if (read_file_to_matrix("input.txt", N, matrixA, t0, X0) == 1) break;
				_getch();
			}

			cout << "\n\n>��������� ������� ������ �� �����.";
			cout << "\n\n �-�� ���������� � ������� ==> " << N << endl;
			cout << " t0 ==> " << t0 << endl;
			_getch();

			break;
		case 3:
			// random
			cout << "\n\n>��������� ������� ������ ���������� ����������.";
			cout << "\n\n ������� �-�� ���������� ==> ";
			N = getNumber();

			rand_to_matrix(N, matrixA, t0, X0);

			break;
		default:
			break;
		}

		SetConsoleTextColor(hStdout, FOREGROUND_BLUE);
		cout << "\n H��������� ����� �������� � ��� t1 ==> ";
		SetConsoleTextColor(hStdout, FOREGROUND_GREEN);
		t1 = getDouble();

		dt = t1 - t0;

		SetConsoleTextColor(hStdout, FOREGROUND_BLUE);
		cout << "\n\n>������ ������� ������������ ���������������� ��������� � ��������� �������������� ������� �����-����� ������� ������� ��������" << endl;
		cout << " (������� ����� ������� . . .)\n";
		_getch();
		
		if (N <= 10)
		{
			SetConsoleTextColor(hStdout, FOREGROUND_BLUE);
			cout << "\n ������������ ������� �������� ����� �������:\n ";
			SetConsoleTextColor(hStdout, FOREGROUND_GREEN);
			coutMatrix(N, matrixA);

			SetConsoleTextColor(hStdout, FOREGROUND_BLUE);
			cout << "\n\n H�������� ������� : ";
			SetConsoleTextColor(hStdout, FOREGROUND_GREEN);
			cout << "\n t0 ==>    " << t0;			
			cout << "\n X0 ==> ";
			coutX0(N, X0);
			cout << "\n t1 ==>    " << t1;

			_getch();
		}

		SetConsoleTextColor(hStdout, FOREGROUND_BLUE);
		cout << "\n ���������, ������� � �������� �������  . . .\n ";

		int result = 0;
		double timeBegin = 0, timeEnd = 0, timeAll = 0, T1 = 0, T2 = 0, speedUp = 0;

		/* ******************************************
		������� ���������������
		******************************************/

		timeBegin = omp_get_wtime();
		result = calcSystemODE(N, matrixA, X0, X, dt);

		timeEnd = omp_get_wtime();
		T1 = timeEnd - timeBegin;

		if (result == 1)
		{
			if (N <= 100)
			{
				SetConsoleTextColor(hStdout, FOREGROUND_BLUE);
				cout << "\n\n ��������� ��� t1 ==> " << t1 << endl;
				SetConsoleTextColor(hStdout, FOREGROUND_GREEN);
				coutResult(N, X);
			}

			SetConsoleTextColor(hStdout, FOREGROUND_BLUE);
			cout << "\n ����� ���������� � �������� ==> ";
			SetConsoleTextColor(hStdout, FOREGROUND_GREEN);
			cout << setprecision(7) << fixed << T1;
		}
		else cout << "Error calc System\n";

		_getch();

		SetConsoleTextColor(hStdout, FOREGROUND_BLUE);
		//cout << "\n ���������  . . .\n ";        

		/* ***************
		OpenMP
		*************** */
		SetConsoleTextColor(hStdout, FOREGROUND_BLUE);
		cout << "\n\n>������ ������� ������������ ���������� � �������������� OpenMP.";
		cout << " (������� ����� ������� . . .)\n";
		_getch();

		int  num_procs = omp_get_num_procs();
		cout << "\n ���������� �����������, ��������� ��� ������������� � ��������� ==> ";
		SetConsoleTextColor(hStdout, FOREGROUND_GREEN);
		cout << num_procs << endl;
		SetConsoleTextColor(hStdout, FOREGROUND_BLUE);
		_getch();

		cout << "\n ���������, ������� � �������� �������  . . .\n ";

		int num_threads = 0;	
		double summ = 0.;

		if (omp_get_dynamic()) omp_set_dynamic(0);
		omp_set_num_threads(4);

		timeBegin = omp_get_wtime();
		result = calcSystemODEParalel(N, matrixA, X0, X, dt);

		timeEnd = omp_get_wtime();
		T1 = timeEnd - timeBegin;

		if (result == 1)
		{
			if (N <= 100)
			{
				SetConsoleTextColor(hStdout, FOREGROUND_BLUE);
				cout << "\n\n ��������� ��� t1 ==> " << t1 << endl;
				SetConsoleTextColor(hStdout, FOREGROUND_GREEN);
				coutResult(N, X);
			}

			SetConsoleTextColor(hStdout, FOREGROUND_BLUE);
			cout << "\n ����� ���������� � �������� ==> ";
			SetConsoleTextColor(hStdout, FOREGROUND_GREEN);
			cout << setprecision(7) << fixed << T1;
		}
		else cout << "Error calc System\n";
			
	/*	
		SetConsoleTextColor(hStdout, FOREGROUND_BLUE);
			cout << " ��������� ==> ";
			SetConsoleTextColor(hStdout, FOREGROUND_GREEN);
			cout << setprecision(5) << fixed << speedUp << endl;	
			*/

		cout << "\n ���������� ��������.\n ";

		_getch();
		SetConsoleTextColor(hStdout, FOREGROUND_RED);
		cout << "\n\n>--------------------------------------------------";
		cout << "\n\t��� ������ - ������� ESC";
		cout << "\n\t��� ������ ������� - ������� ����� �������";

		char c;
		c = _getch();
		if (c == 27) break;
		else continue;
	}

	/*	for (size_t i = 0; i < countX; i++)
	delete[]matrix[i];
	delete[]matrix;
	delete[]x;  */

	return EXIT_SUCCESS;
}