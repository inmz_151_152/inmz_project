#include "stdafx.h"
#include <iostream>
#include <conio.h>
#include <windows.h>
#include <stdio.h>
#include <numeric>
#include <vector>
#include <fstream>
#include <iomanip>
#include <omp.h>

using namespace std;

// ���������� ���� ������ � �������
void SetConsoleTextColor(HANDLE hStdout, int color)
{
	SetConsoleTextAttribute(hStdout, color |
		BACKGROUND_BLUE | BACKGROUND_GREEN | BACKGROUND_RED | BACKGROUND_INTENSITY);
}

// ��������������� ������� ��� ����������� ����� (������ �����)
bool test(char ch, const char *keys, unsigned int size)
{
	for (size_t i = 0; i<size; i++)
		if (keys[i] == ch) return true;
	return false;
}

// ���� ������ ����
int getNumber()
{
	char numbers[11] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '-' };
	string n;
	char ch;

	while ((ch = _getch()) != '\r')//enter
		if (test(ch, numbers, 11))
		{
			n += ch;
			cout << ch;
		}

	cout << endl;
	return atoi(n.c_str());
}

// ���� ������ ����
double getDouble()
{
	char numbers[12] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '-', ',' };
	string n;
	char ch;

	while ((ch = _getch()) != '\r')//enter
	if (test(ch, numbers, 12))
	{
		n += ch;
		cout << ch;
	}

	cout << endl;
	const char* str = n.c_str();
	double res = atof(str);
	return res;
}

// ����� ������ ����
int getMode(unsigned int countMode)
{
	unsigned int mode = 0;
	HANDLE hStdout = GetStdHandle(STD_OUTPUT_HANDLE);
	while (true)
	{
		cout << "\n\n ��� ����� (������� �����) ==> ";
		mode = getNumber();

		if ((mode > countMode) || (mode == 0))
		{
			SetConsoleTextColor(hStdout, FOREGROUND_RED);
			cout << "������� �������� �� ������������� ������� ���� !";
			SetConsoleTextColor(hStdout, FOREGROUND_BLUE);
		}
		else break;
	}

	return mode;
}

// �����������
void coutWelcome()
{
	cout << "\t\t\t\t������\n";
	cout << "\t\t �� ������������� ����������������\n\n\n\n\n";
	cout << "� ������ ������� ����������� ������� ������� ������������ ���������������� ���������\n";
	cout << "� ���������������� ������ � ������������ ������ � ��������������\n";
	cout << "���������� OpenMP\n";
	cout << "\n\n\n��� ����, ����� ������, �������� ����� ������� . . .";
}

// ������ �� �����
int read_file_to_matrix(char *fileName, unsigned int &size, double **&mass, double &t0, double *&X0)
{
	fstream file;
	HANDLE hStdout = GetStdHandle(STD_OUTPUT_HANDLE);
	int temp = 0;

	file.open(fileName, ios::in);
	if (!file.is_open()) // ���� ���� �� ������
	{
		SetConsoleTextColor(hStdout, FOREGROUND_RED);
		cout << "���� �� ����� ���� ������!\n"; // �������� �� ����
		SetConsoleTextColor(hStdout, FOREGROUND_BLUE);
		return 0;
	}
	else
	{
		while (!file.eof()) //���������� � �����
		{
			file >> size;

			mass = new double*[size];
			for (size_t j = 0; j < size; j++)
			{
				mass[j] = new double[size];
			}

			for (unsigned int i = 0; i < size; ++i)
				for (unsigned int j = 0; j < size; ++j)
					file >> mass[i][j];

			file >> t0;
			X0 = new double[size];
			for (unsigned int j = 0; j < size; ++j)
				file >> X0[j];
		}
	}
	file.close();

	return 1;
}

void saveMatrix(unsigned int size, double **mass, double T1)
{
	fstream ff;      //������ �  ����
	ff.open("koef.txt", ios::out);
	ff << T1 << endl;
	ff << size << endl;
	for (unsigned int i = 0; i < size; ++i)
	{
		for (unsigned int j = 0; j < size; ++j)
			ff << mass[i][j] << " ";
	
		if (i != size -1) ff << endl;
	}

	ff.close();
}

// ��������� ��������
int rand_to_matrix(unsigned int &size, double **&mass, double &t0, double *&X0)
{
	mass = new double*[size];
	for (size_t j = 0; j < size; j++)
	{
		mass[j] = new double[size];
	}

	for (unsigned int i = 0; i < size; ++i)
		for (unsigned int j = 0; j < size; ++j)
			mass[i][j] = 1. * (rand() % 3000 - 1500) / 100;

	t0 = 1. * (rand() % 999) / 1000;
	X0 = new double[size];

	for (unsigned int j = 0; j < size; ++j)
		X0[j] = 1. * (rand() % 5000 - 2500) / 1000;

	return 1;
}

// ����� �� ����� �������� 
void coutMatrix(unsigned int size, double **mass)
{
	HANDLE hStdout = GetStdHandle(STD_OUTPUT_HANDLE);
	for (size_t i = 0; i < size; ++i)
	{
		cout << "\n"; 
		for (size_t j = 0; j < size; ++j)
		{
			SetConsoleTextColor(hStdout, FOREGROUND_BLUE);
			cout << setw(8) << setprecision(3) << fixed << mass[i][j];
		}
	}
}

// ����� �� ����� �������� 
void coutX0(unsigned int size, double *mass)
{
	//HANDLE hStdout = GetStdHandle(STD_OUTPUT_HANDLE);
	for (size_t j = 0; j < size; ++j)
	{
		//SetConsoleTextColor(hStdout, FOREGROUND_BLUE);
		cout << setw(8) << setprecision(3) << fixed << mass[j];
	}
}

void coutResult(unsigned int size, double *x)
{
	for (size_t i = 0; i < size; i++)
		cout << "\tX [" << i + 1 << "] = " << setw(10) << setprecision(5) << fixed << x[i] << endl;
}

void enterMatrix(unsigned int size, double **&mass)
{
	HANDLE hStdout = GetStdHandle(STD_OUTPUT_HANDLE);
	for (size_t i = 0; i < size; ++i)
	{
		for (size_t j = 0; j < size; ++j)
		{
			SetConsoleTextColor(hStdout, FOREGROUND_BLUE);
			cout << "\tA [" << i + 1 << "," << j + 1 << "] ==> ";
			SetConsoleTextColor(hStdout, FOREGROUND_GREEN);
			mass[i][j] = getDouble();
		}		
	}
}

void enterX0(unsigned int size, double *&mass)
{
	HANDLE hStdout = GetStdHandle(STD_OUTPUT_HANDLE);
	for (size_t i = 0; i < size; ++i)
	{
		SetConsoleTextColor(hStdout, FOREGROUND_BLUE);
		cout << "\tX0 [" << i + 1 << "] ==> ";
		SetConsoleTextColor(hStdout, FOREGROUND_GREEN);
		mass[i] = getDouble();
	}
}

int calcSystemODE(int N, double **mass, double *x0, double *&x, double dt)
{
	x = new double[N];//��������� ������ ��� �����������
	double *K1 = new double[N];
	double *K2 = new double[N];

	for (int i = 0; i < N; ++i)
	{
		double tempValue = 0;
		for (int j = 0; j < N; ++j)
		{
			tempValue += mass[i][j] * x0[j];
		}

		K1[i] = tempValue;
		tempValue = 0;

		for (int j = 0; j < N; ++j)
		{
			tempValue += mass[i][j] * (x0[j] + 0.5 * dt * K1[i]);
		}

		K2[i] = tempValue;

		x[i] = x0[i] + dt * K2[i];
	}

	return 1;
}

int calcSystemODEParalel(int N, double **mass, double *x0, double *&x, double dt)
{
	x = new double[N];//��������� ������ ��� �����������
	double *K1 = new double[N];
	double *K2 = new double[N];

	for (int i = 0; i < N; ++i)
	{
		double tempValue = 0;

		#pragma omp parallel shared(N, mass)
		{
			#pragma omp for reduction(+: tempValue)
			for (int j = 0; j < N; ++j)
			{
				tempValue += mass[i][j] * x0[j];
			}
		}

		K1[i] = tempValue;
		tempValue = 0;

		#pragma omp parallel shared(N, mass, x0, dt, K1)
		{
			#pragma omp for reduction(+: tempValue)
			for (int j = 0; j < N; ++j)
			{
				tempValue += mass[i][j] * (x0[j] + 0.5 * dt * K1[i]);
			}
		}

		K2[i] = tempValue;

		x[i] = x0[i] + dt * K2[i];
	}

	return 1;
}

double** copyMatrix(unsigned int size, double **mass)
{
	double **newMatrix;
	newMatrix = new double*[size];
	for (size_t j = 0; j<size + 1; j++)
	{
		newMatrix[j] = new double[size + 1];
	}

	for (unsigned int i = 0; i < size; ++i)
		for (unsigned int j = 0; j < size; ++j)
			newMatrix[i][j] = mass[i][j];

	return newMatrix;
}